[Public site](https://vigonoracle.gitlab.io/aap/) for A2P protocol documentation : https://vigonoracle.gitlab.io/aap/

Hardware and implementations can be found in a dedicated project : https://gitlab.com/VigonOracle/measurementchain
