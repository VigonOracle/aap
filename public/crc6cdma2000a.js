//CRC-6/CDMA2000-A https://reveng.sourceforge.io/crc-catalogue/all.htm

function B64ToASCII(val){
  if(typeof(val) != "number") throw "val must be a number (" + val + " provided)";
  if(val >= 64 || val < 0)    throw "value must be in [0-63] interval (" + val + " provided)";
  if(val != Math.round(val))  throw "value must be integer (" + val + " provided)";
  const b64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  return b64Table[val];
}

function ASCIIToB64(char){
  if(typeof(char) != "string")  throw "char must be of type string (" + char + " provided)";
  if(char.length != 1)          throw "char must be a single char (" + char + " provided)";
  const b64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  let result = b64Table.indexOf(char);
  if (result < 0)               throw "char is not a valid Base64 chararcter (" + char + " provided)";
  return result;
}

function crcTableGen(){
  var table = [];
  const generator = 0x27 << 2; //6 bits : must be shifted twice to be left-aligned (Rotate left operations)
  console.log(generator);
  var curr_byte = null;
  for(var divident = 0; divident < 256; divident++){
    curr_byte = divident;
    for(var bit=0; bit<8; bit++){
      if ((curr_byte & 0x80) != 0){
        curr_byte = (curr_byte << 1)%256;
        curr_byte ^= generator;
      }
      else {
        curr_byte = (curr_byte << 1)%256;
      }
    }
    table[divident] = curr_byte;
  }
  return table;
}

function crcCompute(str){
  const crcTable = crcTableGen();
  console.log(crcTable);
  var crc = 0x3F << 2; //align initial value properly
  for(var i=0; i < str.length; i++){
      var curChar = (str.charCodeAt(i))%256;
      var data = (curChar ^ crc) % 256;
      crc = (crcTable[data]) % 256;
  }
  crc >>= 2;
  crc = B64ToASCII(crc);
  console.log( "crc('" + str + "') = base64:'"+crc+"'");
  return (crc);
}
