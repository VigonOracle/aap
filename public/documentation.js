//LICENSE : https://gitlab.com/VigonOracle/aap/-/raw/master/LICENSE
function documentation(){
    return {
        "Orders" : {
            "M!" : {
                "name"   : "Start measurement",
                "manual" : "Start measurement. This function only ask the node to measure value(s), without sending it. Use `D?` or `d?` to collect data.",
                "RFC2119" : ""
            },
            "R!" : {
                "name"   : "Continuous measurement",
                "manual" : "Ask the node to take measurement(s) continuously (without sending them).",
                "RFC2119" : ""
            },
            "x!" : {
                "name"   : "Disable measurements values",
                "manual" : "Will make the node reply a null for every value.",
                "RFC2119" : ""
            },
            "X!" : {
                "name"   : "Reset measurements values.",
                "manual" : "Will make the node take measurements normally back again.",
                "RFC2119" : ""
            },
            "A!" : {
                "name"   : "Change address",
                "manual" : "Change the current node address.",
                "RFC2119" : "The change address command MUST contain the new address to be assigned after the `A` and before the order closure `!` and is thus of the form <code>Ax!</code> where <code>x</code> is the new address to assign to the node."
            },
            "S!" : {
                "name"   : "Go to sleep",
                "manual" : "Go to sleep.",
                "RFC2119" : ""
            }
        },
        "Requests" : {
            "?" : {
                "name"   : "Adress ACK",
                "manual" : "Acknowledge the tested address.",
                "RFC2119": `This command MUST NOT generate any reply.`,
                "return" : ""
            },
            "I?" : {
                "name"   : "Send identifier",
                "manual" : "Send identifier.",
                "RFC2119": `This identifier :
                                <ul>
                                    <li>SHOULD have an <code>id</code> key describing an ID/Serial Number of the module, unique at the institution/enterprise/user level.</li>
                                    <li>MUST have a <code>m</code> key containing an object (or an array of objects if multiple measurements in the same node) that : 
                                        <ul>
                                            <li>MUST have a <code>n</code> [name] key describing the measurement name as a string. That name SHOULD be unique in the network and MUST be unique in the node.</li>
                                            <li>SHOULD have a <code>u</code> [unit] key describing unit as a string, or two keys <code>u</code> and <code>pu</code> describing electrical and physical units respectively.</li>
                                            <li>If <code>u</code> and <code>pu</code> are provided, a <code>tf</code> [transfer function] MUST also be provided as an algebraic expression giving <code>p</code> [physical value] as a function of <code>v</code> [value].</li>
                                        </ul>
                                    </li>
                                    <li>MAY have have other keys, freely decided by user, that MUST be kept constant between measurements.</li>
                                </ul>
                            `,
                "return" : "Object"
            },
            "D?" : {
                "name"   : "Send data [extended]",
                "manual" : "Send data [extended].",
                "RFC2119": `MUST return the same object as the "Send Identifier" [<code>I?</code>] reply, with additional key <code>v</code> in <code>m</code> object (or in each object in <code>m</code> if <code>m</code> is an array of objects).<br>This <code>v</code> key MUST contain the measurement represented as a valid JSON number or null.`,
                "return" : "Object"
            },
            "d?" : {
                "name"   : "Send data [short]",
                "manual" : "Send value (or array of values) reprensenting the data.",
                "RFC2119": `Data MUST be presented in the very same order as in "Send Identifier" [<code>I?</code>] reply if multiple.<br>Each data MUST be a valid JSON number or null.`,
                "return" : "Value or null (or array of numbers or nulls.)"
            }
        }
    };
};
